import Layout from './Layout/Layout';

import Grid from './Grid/Grid';
import GridContainer from './Grid/GridContainer';
import GridContent from './Grid/GridContent';
import GridNavbar from './Grid/GridNavbar';
import GridStatusbar from './Grid/GridStatusbar';
import GridToolbar from './Grid/GridToolbar';


export default {
  install(Vue) {
    Vue.component('m-layout', Layout);

    Vue.component('m-grid', Grid);
    Vue.component('m-container', GridContainer);
    Vue.component('m-content', GridContent);
    Vue.component('m-navbar', GridNavbar);
    Vue.component('m-statusbar', GridStatusbar);
    Vue.component('m-toolbar', GridToolbar);
  },
};
